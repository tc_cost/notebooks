Jupyter notebooks that visualise and play around with the results of the "Social Cost of Tropical Cyclone" paper.

Make sure to install the [scripts](https://gitlab.pik-potsdam.de/tc_cost/tc_cost) and run the full pipeline at least for
the main specification (8 lags). Then, the notebooks need to run in the same conda environment as used by the scripts.